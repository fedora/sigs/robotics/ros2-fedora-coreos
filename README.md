# How to Run the Native Container

Run Fedora CoreOS on your system: https://fedoraproject.org/coreos/download/?stream=stable


Check the details of your system:
```bash
$ rpm-ostree status

    fedora:fedora/x86_64/coreos/stable
                Version: 39.20231101.3.0 (2023-11-20T17:08:57Z)
                Commit: 926547c621e72254a178bdf2dbfd9c094ff88145a91d13e829b4dec7f66fff58
                GPGSignature: Valid signature by E8F23996F23218640CB44CBE75CF5AC418B8E74C
```

Rebase to the Fedora CoreOS + ROS2 container image
```bash
# On my VM and network connection, the rebase process takes around 6 minutes
$ sudo systemctl stop zincati.service
$ sudo rpm-ostree rebase ostree-unverified-registry:quay.io/fedora-sig-robotics/ros2-coreos
```

Reboot the system for the changes
```bash
$ sudo systemctl reboot
```

Verify that we have rebased our system to the container image
```bash
$ rpm-ostree status

    ostree-unverified-registry:quay.io/fedora-sig-robotics/ros2-coreos
                Digest: sha256:926cefbeec20117241c891e06870565d2e5532c164876162b688e2e3ceb20ffa
                Version: 39.20231101.3.0 (2023-11-23T02:37:57Z)
```

Source the ROS2 setup file and run the demo `talker` node
```bash
$ . /etc/ros2/install/local_setup.bash
$ ros2 run demo_nodes_cpp talker
```

Source the ROS2 setup file and run the demo `listener` node
```
$ . /etc/ros2/install/local_setup.bash
$ ros2 run demo_nodes_cpp listener
```
